# digitizer-sis3153-core

This houses the proprietary bits of the SIS3153 VME interface card readout. This exists 
solely as a means to decouple this proprietary software from the other code, which is 
publicly accessible, in the [https://gitlab.cern.ch/faser/daq](https://gitlab.cern.ch/faser/daq)
space as it is used for the readout of the calo/scint digitizer board.
